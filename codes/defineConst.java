// defineConst.java

public class defineConst{
    public static final double pi = 3.14;
    public static final String blog = "PythonDSP"; 
    
    public static void main(String[] args){
        final int radius = 2;
        double area; 

        area = pi * Math.pow(radius, 2);
        System.out.printf("Area of circle = %f\n", area);
        System.out.printf("Blog : %s\n", blog);
    }
}