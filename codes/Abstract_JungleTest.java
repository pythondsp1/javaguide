// Abstract_JungleTest.java

public class Abstract_JungleTest{
    public static void main(String[] args){
        Animal a = new Animal();
        Bird b = new Bird();
        Insect i = new Insect();

        a.scarySound();
        b.scarySound();
        i.scarySound();
    }
}
