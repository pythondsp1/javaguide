// Jungle.java

public class Jungle{

    private String visitorName;

    // Constructor : name should be same as class
    public Jungle(String name){ // constructor with one argument
        visitorName = name;
    }

    // empty constructor : as child class has an empty constructor
    public Jungle(){
    }

    // set (save) visitor name
    public void setVisitorName(String name){
        visitorName = name;
    }

    // get visitor name
    public String getVisitorName(){
        return visitorName;
    }

    // print message 
    public void welcomeMessage(){
        System.out.printf("Hello %s! Welcome to Jungle\n", getVisitorName());
    }
}
