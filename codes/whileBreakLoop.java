// whileBreakLoop.java

public class whileBreakLoop{
    public static void main(String[] args){
    	int i=0;

    	// print 0 to 6 (break at 7)
        while(i < 15){
            System.out.printf("%d, ", i);
            i = i+1;
            if (i%7==0)
            	break;
        }

        System.out.printf("\n");
        System.out.printf("Value of i = %d\n", i); 
    }
}