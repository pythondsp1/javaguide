// arrayEx.java

public class arrayEx{
    public static void main(String[] args){
    	int i;
    	
    	int[] a; // uninitialized array of size 5 i.e. a[0]-a[4]
    	a = new int[10];
    	
        double[] b={2, 4.5, 6}; // initialized array of size 3
        int[] c = {3, 5};
                
        System.out.printf("a[1] = %d\n", a[1]);  // uninitialized array has 0 value

        System.out.printf("c[0] = %d\n", c[0]); // 3


        // print all values of array b	
        // %10s create the width of 10 after 'element' 
        // and 'value' will be printed as right-aligned e.g. see 4.5 in output
        System.out.printf("%s %10s\n", "element", "value");
        for (i=0; i<3; i++){
        	System.out.printf("%4d %12.1f\n", i, b[i]); // %12.1f = show minimum 12 integer & 1 decimal 
        }

        // assign values of array a
        for (i=0; i<5; i++){
          a[i] = 2*i;
        }

        // print values of array a
        System.out.printf("%s %10s\n", "element", "value");
        for (i=0; i<5; i++){
        	System.out.printf("%4d %12d\n", i, a[i]);  // %12.1f = show minimum 12 integer and 1 decimal place 
        }
    }
}