// hexDec.java

class hexDec{
    public static void main(String arg[]){
        int x = 15;
        System.out.printf("x = %d\n", x);

        int h = 0xb;
        int o = 013;
        
        System.out.printf(" hex value of x = %x\n", x); // hex
        System.out.printf(" oct value of x = %o\n", x); // octal
        System.out.printf(" decimal value of x = %d\n\n", x); // decimal

        System.out.printf(" hex value of h = %x\n", h); // hex
        System.out.printf(" oct value of h = %o\n", h); // octal
        System.out.printf(" decimal value of h = %d\n\n", h); // decimal

        System.out.printf(" hex value of o = %x\n", o); // hex
        System.out.printf(" oct value of o = %o\n", o); // octal
        System.out.printf(" decimal value of o = %d\n\n", o); // decimal


    }
}
