// HelloWorld.java

public class HelloWorld{
    public static void main(String[] args){
        System.out.println("Hello World");

        int i = 100;
        double j = 10.10;
        double k;

        k = i + j;

        // System.out.println("k= " + k);
        System.out.print("k= " + k + "\n");

        char c = 'c';                           // char in single quote
        System.out.println("char = " + c);

        boolean b = true;  // true/false
        System.out.println("Boolean b = " + b);
        System.out.println("3>4 :" + (3>4));

        // string is not a datatype; it's a predefined class
        String name = "Meher Krishna Patel";    // string in double quote
        System.out.println("Name = " + name);
    }
}