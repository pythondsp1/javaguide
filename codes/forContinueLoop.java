// forContinueLoop.java

public class forContinueLoop{
    public static void main(String[] args){
    	int i;

    	// print 0 to 4
        for (i=0; i<5; i++){
            if (i%2==0)
            	continue;
            System.out.printf("%d, ", i); //0, 1, 2, 3, 4,

        }

        // value of i is 5
        System.out.printf("\n");
        System.out.printf("Value of i = %d\n", i); // Value of i = 5
    }
}